from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    number = StringField('number', validators=[DataRequired()])
    message = PasswordField('message', validators=[DataRequired()])
    submit = SubmitField('Send Message')
