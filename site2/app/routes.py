from app import app
from flask import Flask, render_template, flash, redirect, request
from flask_mail import Message
import json
import requests

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route("/message", methods=["GET", "POST"])
def my_form():
	if request.method == 'POST':
		ph_number = request.form.get('number')
		message = request.form.get('message')
		r = requests.post("http://api.semaphore.co/api/v4/messages", data={'number': ph_number,'apikey': '64fbd705fce8d36d759f12f55063c833','message': message})
		return("Message Sent")
	return render_template('message.html')

